<?php

class Tradworker_Theme {


	public function __construct() {

		add_action(
			'wp_enqueue_scripts',
			array( &$this, 'do_scripts' )
		);

	}

	public static function do_scripts() {

		$parent_style = 'graphene';

		wp_enqueue_style(
			$parent_style,
			get_template_directory_uri() . '/style.css'
		);

		wp_enqueue_style(
			'tradworker-theme',
			get_stylesheet_directory_uri() . '/style.css',
			array( $parent_style ),
			wp_get_theme()->get('Version')
		);

	}

}

$tradworker_theme = new Tradworker_Theme();

/* EOF */
